#include "window.hpp"
#include <iostream>

Window::Window(std::ostream &outputStream, size_t width, size_t height) : 
    outputStream(outputStream),
    width(width),
    height(height) { }

void Window::redraw(std::pair<size_t, size_t> point, std::vector<p_field> tic, p_field player, std::pair<size_t, size_t> count) {
    std::stringstream ss;
    ss << "     " << count.first << " : " << count.second;
    skore = ss.str();
    refreshText(point, player);
    renderWindow(tic);
}

void Window::redrawWIN(p_field player, bool is_win) {
    if (is_win)
        if (player == wheel)
            outputStream << COLOR_WIN << "\n\r   !!! VYHRALO KOLECKO !!!   \n" << ANSI_COLOR_RESET;
        else
            outputStream << COLOR_WIN << "\n\r   !!! VYHRAL KRIZEK !!!   \n" << ANSI_COLOR_RESET;
    else
        outputStream << COLOR_DRAW << "\n\r   === REMIZA ===   \n" << ANSI_COLOR_RESET;
}

void Window::refreshText(std::pair<size_t, size_t> point, p_field player) {
    std::stringstream ss;
    if (player == wheel)
        ss << "Kolecko: ";
    else
        ss << "Krizek:  ";
    if (point.first)
        ss << point.first << " ";
    if (point.second)
        ss << point.second;
    text = ss.str();
}

void Window::renderWindow(std::vector<p_field> tic) {
    std::stringstream buffer;
    buffer << ANSI_CLEAR << ANSI_COLOR_RESET;
    buffer << "\r";
    for (size_t i = 0; i < 4 * width + 1; ++i)
        buffer << "-";
    buffer << std::endl;
    for (size_t j = 0; j < height; ++j) {
        buffer << "\r";
        for (size_t i = 0; i < width; ++i) {
            buffer << "| ";
            switch (tic[j * width + i]) {
                case nothing: buffer << " "; break;
                case wheel:   buffer << COLOR_GREEN << "O" << ANSI_COLOR_RESET; break;
                case cross:   buffer << COLOR_RED << "X" << ANSI_COLOR_RESET; break;
            default: break;
            }
            buffer << " ";
        };

        buffer << "|";
        if (j == height / 2)
            buffer << skore;       
        buffer << "\n\r";
        for (size_t i = 0; i < 4 * width + 1; ++i)
            buffer << "-";
        buffer << std::endl;
    }
    buffer << "\r" << std::noskipws << text;
    outputStream << buffer.str() << std::endl;;
} 
