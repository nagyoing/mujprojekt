#ifndef TIC_TAC_TOE_ARGPARSER_H
#define TIC_TAC_TOE_ARGPARSER_H

#include <iostream>
#include <string>
#include <cstring>

#define ARG_WIDTH_DEFAULT 3
#define ARG_WIDTH_MIN 3
#define ARG_WIDTH_MAX 9
#define ARG_HEIGHT_DEFAULT 3
#define ARG_HEIGHT_MIN 3
#define ARG_HEIGHT_MAX 9
#define ARG_COUNT_DEFAULT 3
#define ARG_COUNT_MIN 3
#define ARG_COUNT_MAX 9

/*
 * Nastaveni a zruseni raw modu terminalu
 */
void set_raw(bool set);
/*
 * Struktura parametru a nastaveni programu
 */
struct ProgramArguments {
    int width, height, count;
};

class ArgParser {
public:
    /*
     * Parsovani argumentu programu:
     *  -w (-width)
     *  -h (-height)
     *  -c (-count) - pocet znacek v rade
     */
    static ProgramArguments parseProgramArguments(int argc, char *argv[]);
private:
    /*
     * Prevod retezce na cele cislo
     */
    static int str2int(int *res, const char *str);
};  

#endif //TIC_TAC_TOE_ARGPARSER_H
