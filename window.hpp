#ifndef TIC_TAC_TOE_WINDOW_H
#define TIC_TAC_TOE_WINDOW_H

#include <sstream>
#include <vector>

#define ANSI_CLEAR "\x1B[2J\x1B[H"  // mazani terminalu
#define ANSI_COLOR_RESET "\x1B[m"   // reset barev
#define COLOR_RED   "\x1B[91m"      // cervena barva pro krizek
#define COLOR_GREEN "\x1B[92m"      // zelena barva pro kolecko
#define COLOR_WIN   "\x1B[48;5;52m\x1B[38;5;208m"   // oranzova barva na hnedem pozadi pro vypis viteze
#define COLOR_DRAW  "\x1B[48;5;17m\x1B[38;5;75m"    // blede modra barva na modrem pozadi pro vypis viteze

enum p_field {
    nothing,
    wheel,
    cross
};

class Window {
public:
/*
 * Konstruktor a destruktor tridy
 */
    explicit Window(std::ostream &outputStream, size_t width, size_t height);
//    ~Window() { outputStream << "Konec win\n"; };
/*
 * Prekresleni hraciho pole
 *
 * point - souradnice aktualne zadaneho bodu
 * tic - hraci pole
 * player - typ hrace - kolecko/krizek
 * count - casove skore hracu
 */
    void redraw(std::pair<size_t, size_t> point, std::vector<p_field> tic, p_field player, std::pair<size_t, size_t> count);
/*
 * Priprava textu pod hracim polem 
 */
    void refreshText(std::pair<size_t, size_t> point, p_field player);
/*
 * Prekresleni hraciho pole
 */
    void renderWindow(std::vector<p_field> tic);
/*
 * Vypis informaci o vyslednem stavy hry - vyhre a remize
 */
    void redrawWIN(p_field player, bool is_win);
private:
    std::ostream &outputStream;     // odkaz na vystupni proud (std::cout)
    std::string text;               // text vypisovany pod hraci plochou
    std::string skore;              // text o vyslednem stavu hry (vyhra/ remiza)
    size_t width, height;           // rozmery hraci plochy
};

#endif //TIC_TAC_TOE_WINDOW_H
