#include "argparser.hpp"

ProgramArguments ArgParser::parseProgramArguments(int argc, char *argv[]) {
    auto parsedProgramArguments = ProgramArguments{ARG_WIDTH_DEFAULT, ARG_HEIGHT_DEFAULT, ARG_COUNT_DEFAULT};
    for (int i = 1; i < argc; i += 2) {
        int tmp;
        if (std::strlen(argv[i]) > 1)
            switch (argv[i][1]) {
                case 'w':
                    if ((std::strlen(argv[i]) <= 2 || !std::strcmp(argv[i], "-width")) && 
                        str2int(&tmp, argv[i + 1]) >= 0 && tmp >= ARG_WIDTH_MIN && tmp <= ARG_WIDTH_MAX)
                        parsedProgramArguments.width = tmp;
                    break;
                case 'h':
                    if ((std::strlen(argv[i]) <= 2 || !std::strcmp(argv[i], "-height")) && 
                        str2int(&tmp, argv[i + 1]) >= 0 && tmp >= ARG_HEIGHT_MIN && tmp <= ARG_HEIGHT_MAX)
                        parsedProgramArguments.height = tmp;
                    break;
                case 'c':
                    if ((std::strlen(argv[i]) <= 2 || !std::strcmp(argv[i], "-count")) && 
                        str2int(&tmp, argv[i + 1]) >= 0 && tmp >= ARG_COUNT_MIN && tmp <= ARG_COUNT_MAX)
                        parsedProgramArguments.count = tmp;
                    break;
                default: break;
            }
    }
    return parsedProgramArguments;
}
int ArgParser::str2int(int *res, const char *str) {
    try {
        *res = std::stoi(str);
    } catch (const std::invalid_argument &e) {
        return -1;
    } catch (const std::out_of_range &e) {
        return -2;
    }
    return 0;
}

void set_raw(bool set) {
    if (set) {
        system("stty raw");  // enable raw
    } else {
        system("stty -raw"); // disable raw
    }
} 
