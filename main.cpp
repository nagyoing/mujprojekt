#include <iostream>
#include <chrono>
#include <thread>
#include <mutex>
#include <future>
#include <condition_variable>
#include <vector>
#include "window.hpp"
#include "argparser.hpp"

#define PERIOD_COUNTER 200
#define CHANGE_PLAYER(p) p == wheel ? cross : wheel

class tic_tac_toe {
public:
    /*
    * Konstruktor a destruktor tridy tic_tac_toe
    */
    explicit tic_tac_toe(size_t width, size_t height, size_t count) : 
        quit(false),  
        width(width),
        height(height),
        count(count) {
            count_whell = 0; count_cross = 0;
            win = std::make_unique<Window>(std::cout, width, height);
            for (size_t i = 0; i < width * height; ++i)
                tic.push_back(nothing);
            point = std::make_pair(0, 0);
            player = wheel;
            win->redraw(point, tic, player, std::make_pair(count_whell, count_cross));
    }
    ~tic_tac_toe() { control(); }
    /*
     * Kontola shodnosti znacek z bodu point smerem dir
     *
     * point - zacatek cesty
     * dir - zmena smeru, hodnota 1, 0, -1 v obou smerech
     * winn - z daného policka vede vitezna rada znacek nebo takova rada jiz byla predtim nalezena
     * win_pos - z daneho policka vede rada, ktera ma potencial byt "viteznou radou"
     *         - rada obsahuje pouze znacky jednoho typu nebo "nothing"
     */
    void controlDirection(std::pair<size_t, size_t> point, std::pair<int, int> dir, bool *winn, bool *win_pos) {
        p_field tmp = tic[point.second * width + point.first], tmp_wp = tmp;
        size_t sum_w = 0, sum_wp = 0;
        for (size_t i = 0; i < count; ++i) {
            size_t idx = (point.second + dir.second * i) * width + point.first + dir.first * i; 
            if (idx < tic.size()) {
                if (tic[idx] == tmp) ++sum_w;
                if (tmp_wp == nothing) tmp_wp = tic[idx];
                if (tic[idx] == tmp_wp || tic[idx] == nothing) ++sum_wp;
            }
        }
        if (tmp != nothing && sum_w == count) *winn = true;
        if (sum_wp == count) *win_pos = true;
    }
    /*
     * Kontrola shodnosti znacek 
     * - pres vechny body hraci plochy
     * - ve ctyrech zakladnich smerech
     * 
     * Zjistuje, zda existuje "vitezna rada" pro danou znacku
     * Zjistuje take, zda je jeste moznost vytvorit viteznou radu znacek 
     * - pokud ne, hra konic remizou
     */
    void control() {
        p_field pl = CHANGE_PLAYER(player);
        bool winn = false, win_possib = false;
        for (size_t i = 0; i < width; ++i)
            for (size_t j = 0; j < height; ++j) {
                if (i + count <= width)
                    controlDirection(std::make_pair(i, j), std::make_pair(1, 0), &winn, &win_possib);
                if (i + count <= width && j + count <= height)
                    controlDirection(std::make_pair(i, j), std::make_pair(1, 1), &winn, &win_possib);
                if (j + count <= height)
                    controlDirection(std::make_pair(i, j), std::make_pair(0, 1), &winn, &win_possib);
                if (i + 1 >= count && j + count <= height) 
                    controlDirection(std::make_pair(i, j), std::make_pair(-1, 1), &winn, &win_possib);
            }
        if (winn || !win_possib) {
            set_quit(true);
            win->redrawWIN(pl, winn);
        } 
        std::cout << std::endl;
    }
    /*
     * Pridani cisla num do souradnic aktualne zadavaneho bodu point
     * - vcetne kontroly moznosti vlozeni bodu
     */
    void add_coordinates(size_t num) {
        if (!point.first) 
            point = {num, 0};
        else {
            if (!point.second) {
                point.second = num;
                if (!tic[(point.first - 1) * width + num - 1]) {
                    tic[(point.first - 1) * width + num - 1] = player;
                    player = CHANGE_PLAYER(player);
                }
                point = {0, 0};
            }};
    }
    void output_area() {
        std::unique_lock<std::mutex> lg(mutex);
        cvar.wait(lg);
        win->redraw(point, tic, player, std::make_pair(count_whell, count_cross));
    }
    void take() {
        std::this_thread::sleep_for(std::chrono::milliseconds(PERIOD_COUNTER));
        std::unique_lock<std::mutex> lg(mutex);
        if (player == wheel)
            ++count_whell;
        else
            ++count_cross;
        cvar.notify_all();
    }
    void input() {
        std::unique_lock<std::mutex> lg(mutex);
        lg.unlock();
        if (quit)
            return;
        char c;
        std::cin >> c;
        if (c == 'q') {
            set_quit(true); 
            cvar.notify_one();
        } else if (c >= '1' && 
                ((!point.first && c <= '0' + static_cast<char>(height)) || 
                 (point.first && c <= '0' + static_cast<char>(width)))) {
                    add_coordinates(c - '0');
                    control();
                    cvar.notify_one();
        }
    }
    bool get_quit() const {
        return quit;
    }
    void set_quit(const bool qq) {
        quit = qq;
    }
private:
    bool quit;                          // je konec hry?
    std::unique_ptr<Window> win;        
    size_t width, height, count;        // rozmery hraci plochy, pocet znacek v rade pro vyhru
    std::vector<p_field> tic;           // hraci plocha
    std::pair<size_t, size_t> point;    // aktualne zadane souradnice znacky
    size_t count_whell, count_cross;    // casove skore hracu
    p_field player;                     // typ aktualniho hrace
    std::mutex mutex;
    std::condition_variable cvar;
};

int main(int argc, char *argv[]) {
    auto computeThread = [](tic_tac_toe& player) {
        bool q = false;
        while (!q) {
            player.take();
            q = player.get_quit();
        }
    };
    auto outputThread = [](tic_tac_toe& player) {
        bool q = false;
        while (!q) {
            player.output_area();
            q = player.get_quit();
        }
    };
    auto inputThread = [](tic_tac_toe& player) {
        bool q = false;
        while (!q) {
            player.input();
            q = player.get_quit();
        }
    };

    set_raw(true);

    auto param = ArgParser::parseProgramArguments(argc, argv);
    tic_tac_toe PP(param.width, param.height, param.count);
    std::thread t1(computeThread, std::ref(PP));
    std::thread t2(outputThread, std::ref(PP));
    std::thread t3(inputThread, std::ref(PP));

    t1.join();
    t2.join();
    t3.join();
    set_raw(false);

    return 0;
}
