# Hra Piškvorky

## Zadání
Implementujte hru Piškvorky pro dva hráče. 
Hráči se u hry budou střídat. 
Budou postupně zadávat součadnice umístění značky. 
Program automaticky vyhodnotí výsledek hry a tyto imformace zobrazí na terminál.

## Kompilace programu
Program lze zkompilovat následujícím příkazem:
```bash
cmake -Bcmake-build-debug -H. -DCMAKE_BUILD_TYPE=Debug
cmake --build cmake-build-debug
```

## Spuštění a přepínače
Hra se spouští z příkazové řádky.
Parametry nastavení programu:
- -w (-width) - počet sloupců hracího pole,
- -h (-height) - počet řádků hracího pole,
- -c (-count) - požadovaný počet značek pro výhru.
Pokud nejsou parametry programuza dány, jsou využity předefinované hodnoty - hrací pole velikosti 3x3.

Příklady spuštění programu: 
```bash
Piskvorky
Piskvorky -w 5 -h 4 -c 3
Piskvorky -width 7 -c 4 -height 5
Piskvorky -w 7 -h 5
```

## Ovládání programu
Pod hrací plochou je vypsáno, která značka je aktuálně umísťována.

### Zadávání souřadnic
Souřadnice zadáváme jako dvojici `[řádek, sloupec]`. Řádky i sloupce jsou číslovány od 1. 

### Ukončení programu
Program je ukončí automaticky, pokud jeden z hráčů vytvoří souvislou řadu značek v jednom z definovaných směrů. Hra končí i tehdy, pokud již není možné dosáhnout vítězství na kterékoliv straně.

Program je možné kdykoliv ukončit klávesou `Q`.

## Testování programu

### Příklad testů pro hru Piškvorky (rozměry 3x3):
-	`q`
-	`1q`
-	`12q`
-	`112233q`
-	`1121223133`		- vyhrává kolečko (hlavní diagonála)
-	`122113112231`		- vyhrává křížek (sloupec vlevo)
-	`1221223233111331`	- vyhrává křížek (sloupec vlevo)
-	`112112223123`		- vyhrává křížek (prostřední řádek)
-	`311122213113`		- vyhrává kolečko (vedlejší diagonála), kolečko – chybné zadání
-	`11121321222331`	- vyhrává kolečko (vedlejší diagonála)
-	`111222333121321323`	- remíza
-	`1121311233222332`	- vyhrává křížek (prostřední sloupec)
-	`13s2223z1133`		- vyhrává kolečko (sloupec vpravo)
-	`1121122213`		- vyhrává kolečko (horní řádek)
-	`3122211133132232` 	- vyhrává kolečko (dolní řádek), kolečko – chybné zadání
-	`1122213113123223`	- remíza
### Příklad testů pro hru Piškvorky (rozměry 7x5, 4 značky v řadě):
-	`112212132333344521445553463526154325`		- vyhrává křížek
-	`112212231314243515173351162644534352544527`	- remíza
### Příklad testů pro hru Piškvorky (rozměry 7x5, 5 značek v řadě):
-	`11221223212431252613415133353246574217522753544337474445151634`	- remíza
-	`1122122321243125261341513335324657421752275354433747441516453455`	- vyhrává křížek
-	`1122122313142435151733511626445343525445273442`			- vyhrává kolečko
